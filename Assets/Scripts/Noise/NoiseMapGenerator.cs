﻿using System;
using UnityEngine;

public class NoiseMapGenerator
{
    private int width;
    private int seed;
    private float isoLevel;
    private float mergingRatio;
    private float scale;
    private float lacunarity;
    private int octaves;
    private int detail;

    private System.Random noise;
    //private float maxNoiseHeight = 0;

    public ComputeShader noiseShader;


    private static NoiseMapGenerator instance;
    
    public static void initInstance(float isoLevel, int detail,
        int width, float scale, int seed, int octaves, float mergingRatio, float lacunarity,
        ComputeShader noiseShader)
    {
        instance = new NoiseMapGenerator(isoLevel, detail, width, scale, seed, octaves, mergingRatio, lacunarity, noiseShader);
    }

    public static NoiseMapGenerator getInstance() 
    {
        return instance; 
    }

    private NoiseMapGenerator(
        float isoLevel, int detail,
        int width, float scale, int seed, int octaves, float mergingRatio, float lacunarity,
        ComputeShader noiseShader)
    {
        this.width = width;
        this.seed = seed;
        this.isoLevel = isoLevel;
        this.mergingRatio = mergingRatio;
        this.scale = scale > 0f ? scale : 0.0001f;
        this.noiseShader = noiseShader;
        this.octaves = octaves;
        
        this.lacunarity = lacunarity;
        this.detail = detail;
    }

    public float[] get3DNoise(int posX, int posY, int posZ, int mult)
    {
        int length = detail + 1;
        float[,,] grid = new float[length, length, length];

        Vector3 pos = new Vector3(posX, posY, posZ) * width;
        float offsetDetail = (float)(width * mult) / (float)(detail - 2);
        pos -= Vector3.one * offsetDetail;

        float[] data = new float[length * length * length];
        ComputeBuffer noiseBuffer = new ComputeBuffer(data.Length, sizeof(float));
        
        float[] globalData = new float[2];
        globalData[0] = 0; //counter de case au dessus de l'isolevel
        globalData[1] = 0;  //1 si le terrain est rélié a un autre chunk
        ComputeBuffer globalBuffer = new ComputeBuffer(globalData.Length, sizeof(float));

        noiseBuffer.SetData(data);
        noiseShader.SetBuffer(0, "Result", noiseBuffer);
        
        globalBuffer.SetData(globalData);
        noiseShader.SetBuffer(0, "GlobalResult", globalBuffer);
        
        noiseShader.SetInt("length", length);
        noiseShader.SetFloat("isoLevel", isoLevel);

        noiseShader.SetInt("seed", seed);
        noiseShader.SetFloat("scale", scale);
        noiseShader.SetFloat("mergingRatio", mergingRatio);
        noiseShader.SetFloat("offset", offsetDetail);
        noiseShader.SetInt("octaves", octaves);
        noiseShader.SetFloat("lacunarity", lacunarity);

        noiseShader.SetFloat("xBase", pos.x);
        noiseShader.SetFloat("yBase", pos.y);
        noiseShader.SetFloat("zBase", pos.z);

        noiseShader.Dispatch(0, data.Length / 8, 1, 1);

        noiseBuffer.GetData(data);
        globalBuffer.GetData(globalData);
        globalBuffer.Release();
        noiseBuffer.Release();
        return data;
    }
}
