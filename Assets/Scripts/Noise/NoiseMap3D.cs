﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMap3D
{
    private int width;
    private float scale;
    private float persistance;
    private float lacunarity;

    private System.Random perlinRandom;

    private Vector3 offset;

    public NoiseMap3D(int width,int seed)
    {
        this.width = width + 1;
        this.perlinRandom = new System.Random(seed);

        this.offset = new Vector3(
            perlinRandom.Next(-100000, 100000),
            perlinRandom.Next(-100000, 100000),
            perlinRandom.Next(-100000, 100000)
        );
    }

    public float[,,] generateGrid(int x, int y ,int z)
    {
        float[,,] grid = new float[width, width, width];
        for(int i = 0; i < width; i++)
            for (int j = 0; j < width; j++)
                for (int k = 0; k < width; k++)
                {
                    getPerlinValue(i + offset.x + x, j + offset.y + y, k + offset.z + z);
                }
        return grid;
    }

    private float getPerlinValue(float x, float y, float z)
    {
        float ab = Mathf.PerlinNoise(x, y);
        float bc = Mathf.PerlinNoise(y, z);
        float ac = Mathf.PerlinNoise(x, z);

        float ba = Mathf.PerlinNoise(y, x);
        float cb = Mathf.PerlinNoise(z, y);
        float ca = Mathf.PerlinNoise(z, x);

        float abc = ab + bc + ac + ba + cb + ca;
        return abc / 6f;
    }
}
