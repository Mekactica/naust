﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldGenerator : MonoBehaviour
{
    [Header("General")]
    public int seed = 57899264;
    public int renderDistance = 8;
    public int detail = 30;
    public int chunkWidth = 40;
    public bool ComplexMesh;
    public ComputeShader noiseShader;
    public ComputeShader meshShader;
    public Text log;
    [Range(0, 1)] public float isolevel = .56f;

    public GameObject meshPrefab;
    public Vector3Int pos;

    [Header("Map Noise")]
    public float mergingRatio = 3;
    public int Width = 40;
    public float Scale = 27.6f;
    public int Octaves = 5  ;
    public float Lacunarity = 1.87f;

    private ChunkManager chunkManager;


    private void Awake()
    {
        detail += 2;
        chunkManager = ChunkManager.getInstance();
        NoiseMapGenerator.initInstance(isolevel, detail, Width, Scale, seed, Octaves, mergingRatio, Lacunarity, noiseShader);
        Chunk.detail = detail;
        ChunkMeshGenerator.isoLevel = isolevel;
        ChunkMeshGenerator.meshShader = meshShader;
        ChunkMeshGenerator.complexMesh = ComplexMesh;

        chunkManager.init(chunkWidth, renderDistance, meshPrefab, log);
    }

    public void Update()
    {
        chunkManager.updateChunkInQueue();
        chunkManager.logChunkInQueue();
    }
}
