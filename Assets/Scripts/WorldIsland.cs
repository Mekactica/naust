﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldIsland : MonoBehaviour
{
    [Header("General")]
    public int seed = 57899264;
    public int detail = 30;
    public bool ComplexMesh;
    public ComputeShader noiseShader;
    public ComputeShader meshShader;
    [Range(0, 1)] public float isolevel = .56f;

    public GameObject meshPrefab;

    [Header("Map Noise")]
    public float mergingRatio = 3;
    public int Width = 40;
    public float Scale = 27.6f;
    public int Octaves = 5;
    public float Lacunarity = 1.87f;


    private void Awake()
    {
        detail += 2;
        NoiseMapGenerator.initInstance(isolevel, detail, Width, Scale, seed, Octaves, mergingRatio, Lacunarity, noiseShader);
        ChunkMeshGenerator.isoLevel = isolevel;
        ChunkMeshGenerator.meshShader = meshShader;
        ChunkMeshGenerator.complexMesh = ComplexMesh;
    }
}
