﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandManager : MonoBehaviour
{
    public int maxLayer;
    private int detail;
    private float isoLevel;
    private NoiseMapGenerator noiseMapGenerator;

    private static GameObject world = GameObject.Find("World");
    public Vector3Int playerPos;

    public bool loading = true;
    public bool update = false;
    public Vector3 direction;

    private List<Island> islands = new List<Island>(); 

    public static IslandManager instance;

    public static IslandManager getInstance()
    {
        if (instance == null)
            instance = new IslandManager();
        return instance;
    }

    public IslandManager() { }

    public void init(int maxLayer, float isolevel, int detail, NoiseMapGenerator noiseMapGenerator)
    {
        this.maxLayer = maxLayer;
        this.isoLevel = isolevel;
        this.noiseMapGenerator = noiseMapGenerator;
        this.detail = detail;
    }

    public void ScanIsland(int posX, int posY, int posZ, int mult)
    {

        noiseMapGenerator.get3DNoise(posX,posY,posZ, mult);
    }
}
