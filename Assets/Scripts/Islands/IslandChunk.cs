﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Islands
{
    public class IslandChunk : MonoBehaviour
    {
        private Island island;

        public int id;
        public int distance;
        public bool isLoaded;
        public int[,] coordinates;


        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public IslandChunk(Island island, int id)
        {
            this.island = island;
            this.id = id;
        }

        private void save()
        {

        }

        private void setVisible(bool setVisible)
        {

        }

        private void updateMesh()
        {

        }

    }
}
