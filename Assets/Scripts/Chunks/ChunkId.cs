﻿using UnityEngine;

public class ChunkId
{
    public int x { get; set; }
    public int y { get; set; }
    public int z { get; set; }

    public ChunkId(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public string toString()
    { 
        return $"{x}:{y}:{z}";
    }

    public ChunkId getOpposite(int maxDistance, int[] distance)
    {
        int spacing = (maxDistance * 2 + 1) * distance[1];
        switch (distance[2])
        {
            case 0: return new ChunkId(x - spacing, y, z);
            case 1: return new ChunkId(x, y - spacing, z);
            default: return new ChunkId(x, y, z - spacing);
        }
    }

    public int maxDistance(ChunkId otherChunk)
    {
        return Mathf.Max(Mathf.Abs(this.x - otherChunk.x), Mathf.Abs(this.y - otherChunk.y), Mathf.Abs(this.z - otherChunk.z));
    }

    public void updateId(Vector3 diff)
    {
        x += (int)diff.x;
        y += (int)diff.y;
        z += (int)diff.z;
    }

    public Vector3 toVector3()
    {
        return new Vector3(x, y, z);
    }

    public bool isInAxis(int coor, char axis)
    {
        return (x == coor && axis == 'x') || (y == coor && axis == 'y') || (z == coor && axis == 'z');
    }
}
