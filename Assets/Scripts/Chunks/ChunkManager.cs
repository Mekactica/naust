﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ChunkManager
{
    public int maxLayer;
    private int chunkSize;
    private Text logUi;

    private GameObject chunkPrefab;
    private static GameObject world = GameObject.Find("World");

    private Dictionary<int,Queue<Chunk>> updateQueue = new Dictionary<int, Queue<Chunk>>();

    public Vector3Int playerPos;

    public bool loading = true;
    public bool update = false;
    public Vector3 direction;

    private int nbFrames = 0;

    private Dictionary<int, GameObject> layers = new Dictionary<int, GameObject>();

    public List<string> chunkToDel = new List<string>();

    public static ChunkManager instance;

    public static ChunkManager getInstance()
    {
        if (instance == null)
            instance = new ChunkManager();
        return instance;
    }

    public ChunkManager(){}

    public void init(int chunkSize, int maxLayer, GameObject chunkPrefab, Text logUi)
    {
        this.chunkSize = chunkSize;
        this.maxLayer = maxLayer;
        this.chunkPrefab = chunkPrefab;
        this.logUi = logUi;
        for (int i = 1; i <= maxLayer; i++)
        {
            updateQueue.Add(i, new Queue<Chunk>());
        }
    }

    public void loadMap(Vector3 pos)
    {
        playerPos = Vector3Int.FloorToInt(pos / chunkSize);

        ChunkId cId = new ChunkId(0, 0, 0);
        GameObject island = GameObject.Instantiate(chunkPrefab);
        island.GetComponent<Chunk>().initChunk(cId, chunkSize, 1, 1);

        int nbChunk = 1;
        int mult = 0;
        int[] ids = new int[] { playerPos.x, playerPos.y, playerPos.z };
        for (int layer = 1; layer <= maxLayer; layer++)
        {
            mult = (int)Mathf.Pow(3, layer - 1);
            ids[0] -= mult;
            ids[1] = ids[0] + mult;
            ids[2] = ids[1] + mult;
            for (int x = 0; x <= 2; x++)
            for (int y = 0; y <= 2; y++)
            for (int z = 0; z <= 2; z++)
                    {
                        if (x == 1 && y == 1 && z == 1) continue;
                        cId = new ChunkId(ids[x], ids[y], ids[z]);
                        island = GameObject.Instantiate(chunkPrefab);
                        island.GetComponent<Chunk>().initChunk(cId, chunkSize, layer, mult);
                        nbChunk++;
                    }
        }
        loading = false;
        logUi.text = "Loading complete : " + nbChunk + " loaded";
    }
    
    public GameObject getLayer(int layer)
    {
        if (!layers.ContainsKey(layer))
        {
            layers.Add(layer, new GameObject());
            layers[layer].transform.SetParent(world.transform);
            layers[layer].transform.name = "Layer " + layer;
        }
        return layers[layer];
    }

    public void updatePos(Vector3 pos)
    {
        if (playerPos == null || loading) return;
        Vector3Int newPlayerPos = Vector3Int.FloorToInt(pos / chunkSize);
        if (playerPos == newPlayerPos)
        {
            this.update = false;
        }
        else
        {
            this.direction =  newPlayerPos - this.playerPos;
            this.playerPos = newPlayerPos;
            this.update = true;
        }
    }

    private int getPos(char axis)
    {
        return axis == 'x' ? playerPos.x : axis == 'y' ? playerPos.y : playerPos.z;
    }

    public void addChunkToQueue(Chunk ch)
    {
        updateQueue[ch.layer].Enqueue(ch);
        ch.inQueue = true;
        int total = 0;
        for (int i = 1; i <= maxLayer; i++)
        {
            total += updateQueue[i].Count;
        }
        logUi.text = "Layers : " + 4 + " | Updating "+ total +" chunks...";
    }

    public void updateChunkInQueue()
    {
        if (nbFrames == 500) nbFrames = 0;
        nbFrames++;

        Chunk updateChunk;
        for (int i = maxLayer; i >= 1; i--)
        {
            if (nbFrames % (i * i * 5) == 0 && updateQueue[i].Count != 0)
            {
                updateChunk = updateQueue[i].Dequeue();
                updateChunk.inQueue = false;
                updateChunk.updateChunk();
                return;
            }
        }
        
        if (updateQueue.ContainsKey(1) && updateQueue[1].Count != 0)
        {
            updateChunk = updateQueue[1].Dequeue();
            updateChunk.inQueue = false;
            updateChunk.updateChunk();
        }
        
    }

    public void logChunkInQueue()
    {
        int total = 0;
        for (int i = 1; i <= maxLayer; i++)
        {
            total += updateQueue[i].Count;
        }
        logUi.text = "Layers : " + maxLayer + " | Updating " + total + " chunks...";
    }
}
