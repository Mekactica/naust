﻿using System;
using UnityEngine;

public class Chunk : MonoBehaviour
{
    public static GameObject prefab;

    private ChunkId id;
    public static int detail;
    private int chunkSize;
    private Vector3 offset;

    private ChunkManager chunkManager;

    public int layer;
    public int multiple;

    // update values
    public bool inQueue;
    public Vector3 centerOffset;
    private ChunkId currentIdLoaded;


    // mesh config
    private MeshRenderer mr;
    private MeshFilter mf;
    private MeshCollider mc;

    private float[] grid;

    private void Start()
    {
        chunkManager = ChunkManager.getInstance();
        mr = transform.GetComponent<MeshRenderer>();
        mc = transform.GetComponent<MeshCollider>();
        mr.material = new Material(Shader.Find("Shader Graphs/TerrainShader"));
        mf = transform.GetComponent<MeshFilter>();
        mf.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mc.sharedMesh = mf.mesh;

    }
    public void forceRender()
    {
        ChunkMeshGenerator cmg = ChunkMeshGenerator.getInstance();
        cmg.calculateMeshShader(mf.mesh, grid, detail);
    }


    private void Update()
    {
        if (chunkManager.loading || id == null) return;

        if (chunkManager.update && id != null)
        {
            Vector3 direction = chunkManager.direction;
            Vector3Int playerPos = chunkManager.playerPos;

            if (layer == 1)
            {
                Vector3 idV3 = id.toVector3();
                if ((direction.x != 0 && idV3.x == playerPos.x - direction.x * 2) ||
                    (direction.y != 0 && idV3.y == playerPos.y - direction.y * 2) ||
                    (direction.z != 0 && idV3.z == playerPos.z - direction.z * 2))
                {
                    updateId(chunkManager.direction * 3);
                }
            }
            else
            {
                updateId(chunkManager.direction);
            }
        }
        if (!inQueue && chunkManager.update && currentIdLoaded.maxDistance(id) >= multiple/6)
        {
            chunkManager.addChunkToQueue(this);
        }
    }

    public void updateChunk()
    {
        transform.position = id.toVector3() * (chunkSize - 1) + offset;
        grid = NoiseMapGenerator.getInstance().get3DNoise(id.x, id.y, id.z, this.multiple);

        ChunkMeshGenerator cmg = ChunkMeshGenerator.getInstance();
        cmg.calculateMeshShader(mf.mesh, grid, detail);

        currentIdLoaded = new ChunkId(id.x, id.y, id.z);
    }

    public ChunkId getId() { return id; }

    private void updateId(Vector3 diff) {
        id.updateId(diff);
        transform.name = id.toString();
    }

    public void initChunk(ChunkId id, int chunkSize, int layer, int multiple)
    {
        Start();

        this.id = id;
        this.centerOffset = new Vector3(chunkSize / 2, chunkSize / 2, chunkSize / 2);
        this.currentIdLoaded = new ChunkId(id.x,id.y,id.z);
        transform.name = id.toString(); 
        transform.SetParent(chunkManager.getLayer(layer).transform);
        this.layer = layer;
        this.multiple = multiple;
        this.chunkManager = ChunkManager.getInstance();

        this.chunkSize = chunkSize;
        
        offset = -Vector3.one * ((float)(chunkSize-1)/(float)detail) * (float)multiple;

        transform.localScale = Vector3.one * ((float)(chunkSize-1)/(float)(detail-2)) * (float)multiple;
        transform.position = new Vector3(id.x, id.y, id.z) * (chunkSize - 1) + offset;

        this.grid = NoiseMapGenerator.getInstance().get3DNoise(id.x, id.y, id.z, this.multiple);
        mc.enabled = layer == 1;

        ChunkMeshGenerator cmg = ChunkMeshGenerator.getInstance();
        cmg.calculateMeshShader(mf.mesh, grid, detail);
    }
}
