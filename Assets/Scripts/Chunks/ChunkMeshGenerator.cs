﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChunkMeshGenerator
{
    public static float isoLevel = .65f;
    public static bool complexMesh;
    public static ComputeShader meshShader;

    private static Color ground = new Color(.43f, .78f, .18f);
    private static Color rock = new Color(.70f, .70f, .70f);
    private static Color underground = new Color(.54f, .36f, .25f);
    private static GradientColorKey[] gradientColorKeys;
    private static GradientAlphaKey[] gradientAlphaKeys;
    private static Gradient gradientTerrain;

    private struct Triangle
    {
        #pragma warning disable 649 // disable unitialized warning in editor
        public Vector3 point1;
        public Vector3 point2;
        public Vector3 point3;
        public float slope;

        public Vector3 getPoint(int i)
        {
            switch (i)
            {
                case 1: return point1;
                case 2: return point2;
                case 3: return point3;
                default: return Vector3.zero;
            }
        }
        public Color getColor()
        {
            if (float.IsNaN(slope)) return ground;
            return gradientTerrain.Evaluate(slope / 3.15f);
        }
    }

    private class MeshData
    {
        public Vector3[] vertices;
        public Color[] colors;
        public int[] triangles;

        public void SetComplexMesh(Triangle[] shaderOutput)
        {
            var listVertices = new List<Vector3>();
            var listColors = new List<Color>();
            var listTriangles = new List<int>();

            int i = 0;
            foreach (Triangle output in shaderOutput)
            {
                for (int j = 1; j <= 3; j++)
                {
                    listVertices.Add(output.getPoint(j));
                    listColors.Add(output.getColor());
                    listTriangles.Add(i * 3 + j - 1);
                }
                i++;
            }

            vertices = listVertices.ToArray();
            colors = listColors.ToArray();
            triangles = listTriangles.ToArray();
        }
        public void SetSimpleMesh(Triangle[] shaderOutput)
        {
            var listVertices = new List<Vector3>();
            var listColors = new List<Color>();
            var listTriangles = new List<int>();
            var checkVert = new Dictionary<Vector3, int>();

            int i = 0;
            foreach (Triangle output in shaderOutput)
            {
                for (int j = 1; j <= 3; j++)
                {
                    if (!checkVert.ContainsKey(output.getPoint(j)))
                    {
                        listVertices.Add(output.getPoint(j));
                        listColors.Add(output.getColor());

                        checkVert[output.getPoint(j)] = vertices.Length - 1;
                    }
                    listTriangles.Add(checkVert[output.getPoint(j)]);
                }
                i++;
            }
        }
    }

    public static ChunkMeshGenerator instance;

    public static ChunkMeshGenerator getInstance()
    {
        if (instance == null)
            instance = new ChunkMeshGenerator();
        return instance;
    }

    private ChunkMeshGenerator()
    {
        gradientTerrain = new Gradient();
        gradientColorKeys = new GradientColorKey[3];
        gradientColorKeys[0].color = ground;
        gradientColorKeys[0].time = 0f;
        gradientColorKeys[1].color = underground;
        gradientColorKeys[1].time = 0.4f;
        gradientColorKeys[2].color = rock;
        gradientColorKeys[2].time = 0.80f;

        gradientAlphaKeys = new GradientAlphaKey[2];
        gradientAlphaKeys[0].alpha = 1.0f;
        gradientAlphaKeys[0].time = 0.0f;
        gradientAlphaKeys[1].alpha = 1.0f;
        gradientAlphaKeys[1].time = 1.0f;

        gradientTerrain.SetKeys(gradientColorKeys, gradientAlphaKeys);
    }

    public void calculateMeshShader(Mesh mesh, float[] grid, int axisLength)
    {
        int maxTriangles = grid.Length * 5;
        ComputeBuffer resultBuffer = new ComputeBuffer(maxTriangles, sizeof(float) * 10, ComputeBufferType.Append); // float * 10 because 3 vector3 + 1 float
        ComputeBuffer gridBuffer = new ComputeBuffer(grid.Length, sizeof(float));

        resultBuffer.SetCounterValue(0);
        gridBuffer.SetData(grid);
        meshShader.SetBuffer(0, "grid", gridBuffer);
        meshShader.SetBuffer(0, "Result", resultBuffer);
        meshShader.SetFloat("isoLevel", isoLevel);
        meshShader.SetInt("axisLength", axisLength + 1);

        int threadsPerAxis = Mathf.CeilToInt(axisLength / 8f);
        meshShader.Dispatch(0, threadsPerAxis, threadsPerAxis, threadsPerAxis);

        ComputeBuffer countBuffer = new ComputeBuffer(1, sizeof(int), ComputeBufferType.Raw);
        ComputeBuffer.CopyCount(resultBuffer, countBuffer, 0);
        int[] countArray = { 0 };
        countBuffer.GetData(countArray);
        int nbTriangles = countArray[0];

        Triangle[] triangles = new Triangle[nbTriangles];
        resultBuffer.GetData(triangles);

        resultBuffer.Release();
        gridBuffer.Release();
        countBuffer.Release();

        MeshData meshData = new MeshData();
        meshData.SetComplexMesh(triangles);

        updateMesh(mesh, meshData);
    }

    private void updateMesh(Mesh mesh, MeshData meshData)
    {
        mesh.Clear();
        if (meshData.vertices.Length == 0) return;
        mesh.vertices = meshData.vertices;
        mesh.colors = meshData.colors;
        mesh.triangles = meshData.triangles;

        mesh.Optimize();
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
    }
}
