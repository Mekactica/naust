﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody rb;
    public Camera mCam;
    public ChunkManager wr;
    public int speed = 10;

    private bool isFlying = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        initWorld();
    }

    public void initWorld()
    {
        wr = ChunkManager.getInstance();
        wr.loadMap(rb.position);
    }

    public void updateWorld()
    {
        wr.updatePos(rb.position);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isFlying = !isFlying;
        }
        if (isFlying) flyingMovement();
        updateWorld();
    }

    private void flyingMovement()
    {
        rb.velocity = transform.forward * speed;

        // Smoothly tilts a transform towards a target rotation.
        float tiltAroundY = Input.GetAxis("Horizontal") * .5f;
        float tiltAroundX = Input.GetAxis("Vertical") * .5f;

        if (tiltAroundX != 0 || tiltAroundY != 0)
        {
            // Rotate the cube by converting the angles into a quaternion.
            Quaternion target = Quaternion.Euler(tiltAroundX, tiltAroundY, 0);

            // Dampen towards the target rotation
            transform.Rotate(Vector3.left * tiltAroundX);
            transform.Rotate(Vector3.up * tiltAroundY);
        }
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
    }
}
