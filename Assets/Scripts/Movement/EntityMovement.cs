﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityMovement : MonoBehaviour
{
    public Rigidbody rb;
    public int speed;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }
    
    public Vector3 getPosition()
    {
        return rb.position;
    }



    private void flyingMovement()
    {
        rb.velocity = transform.forward * speed;

        // Smoothly tilts a transform towards a target rotation.
        float tiltAroundY = Input.GetAxis("Horizontal") * 1f;
        float tiltAroundX = Input.GetAxis("Vertical") * 1f;

        if (tiltAroundX != 0 || tiltAroundY != 0)
        {
            // Rotate the cube by converting the angles into a quaternion.
            Quaternion target = Quaternion.Euler(tiltAroundX, tiltAroundY, 0);

            // Dampen towards the target rotation
            transform.Rotate(Vector3.left * tiltAroundX);
            transform.Rotate(Vector3.up * tiltAroundY);
        }
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
    }
}
