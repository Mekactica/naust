# Projet Naust

Après avoir découvert cette vidéo présentant l'algorithme des marching cubes : https://www.youtube.com/watch?v=M3iI2l0ltbE, et avoir réalisé des recherches sur le sujet, j'ai eu l'idée de tenter de créer une génération procédurale d'un monde constitué d'îles volantes.

C'est ainsi que le projet Naust est né : un jeu où le joueur aura pour but d'explorer à toute vitesse un ciel rempli d'îles volantes.

Pour le moment, ce projet me sert surtout à réaliser des expérimentations sur la génération de terrain, les contrôles du personnage et les visuels étant pour l'instant très basiques. Voici un exemple de la génération à l'état actuel :

![](https://gitlab.com/Mekactica/naust/-/raw/master/ReadMeFiles/visite_monde_avec_chunk.mp4)

Je ne prévois pas pour l'instant de publier ce jeu, il me sert principalement de terrain d'expérimentations.

Mes scripts C# sont présents dans [/Assets/Scripts](/Assets/Scripts)
Les shaders (pour la génération de terrain et de bruit) sont présents dans [/Assets/Shaders](/Assets/Shaders)


## Génération de terrain

Le personnage ayant pour but d'explorer à grande vitesse, il me faut donc générer un grand nombre d'îles volantes qu'il pourra croiser sur son chemin.
Il n'est donc pas envisageable de créer ces îles à la main.

Pour ce faire, j'ai mis en place une génération de terrain en deux étapes : 
1. Une simulation de bruit pour pouvoir déterminer si à un endroit spécifique, nous aurons de la terre ou de l'air.
2. Une génération de mesh pour pouvoir créer les objets 3D représentants les îles.

### La simulation de bruit

Pour le bruit, j'utilise du __*Cellular Noise*__ pour générer l'emplacement des différentes îles puis une surcouche de __*Value Noise*__ pour les détails du terrain.
Enfin, pour générer des réseaux de cavernes j'utilise en plus du __*Simplex Noise*__.

Le bruit sera représenté dans une grille en 3 dimensions, chaque point déterminant la valeur du sol à cet endroit précis.

Le fichier permettant de générer mes grilles de bruits est [MapNoise.compute](/Assets/Shader/MapNoise.compute). Le programme C# appelant est [NoiseMapGenerator.cs](Assets/Scripts/Noise/NoiseMapGenerator.cs)

Pour pouvoir générer ces bruits, je me base sur la librairie open source [FastNoise Lite](https://github.com/Auburn/FastNoiseLite).

| Cellular Noise | Value Noise | Simplex Noise |
|---|---|---|
|![Cellular Noise](https://gitlab.com/Mekactica/naust/-/raw/master/ReadMeFiles/cellular_noise.gif) | ![Value Noise](https://gitlab.com/Mekactica/naust/-/raw/master/ReadMeFiles/value_noise.gif) | ![Simplex Noise](https://gitlab.com/Mekactica/naust/-/raw/master/ReadMeFiles/cave_noise.gif) |

Pour obtenir ces grilles, j'y ai altéré plusieurs paramètres pour mieux convenir aux besoins que j'avais.

### La génération de mesh 
Pour la génération de mesh je me suis basé sur l'algorithme des [marching cubes](https://fr.wikipedia.org/wiki/Marching_cubes).

Je détermine d'abord une valeur entre 0 et 1 qui indiquera la frontière en air et sol.
En utilisant la grille de bruit généré précédemment, l'algorithme des marching cubes parcourra la grille et représentera la frontière en 3 dimensions entre l'air et le sol avec notre valeur.
Pour ce faire, il compare les valeurs des cases voisines et utilise une table d'association afin de déterminer les triangles à générer dans la mesh du terrain.

Mon implémentation de l'algorithme se trouve dans [MeshGenerator.compute](Assets/Shader/MeshGenerator.compute) et les tables de comparaison nécessaire sont dans [MarchingData.hlsl](Assets/Shader/MarchingData.hlsl). Le programme C# appelant est [ChunkMeshGenerator.cs](Assets/Scripts/Chunks/ChunkMeshGenerator.cs).

## Performance

### Shaders
La génération de bruit et de mesh demande un grand nombre de calculs mathématiques, pour générer une grille en 3 dimensions, il y a dimension^3 d'opérations à réaliser.

Par défaut, Unity exécute ces calculs sur le processeur, qui peut avoir des soucis de rapidité lorsque la dimension des grilles augmentent.

Pour résoudre ce problème, j'ai décidé de mettre en place des shaders afin de réaliser les générations sur le processeur graphique. Celui-ci est beaucoup plus rapide pour réaliser ce type de répétitions.

Vous pouvez retrouver les shaders [ici](/Assets/Shader)


### Génération par chunk

Il n'est pas possible pour une machine de supporter une génération infinie d'îles immédiates.

J'ai donc mis en place un système de chunk, c'est-à-dire que la génération de terrain sera divisé en plusieurs régions.

Contrairement, à une génération procédurale classique ou générale le terrain est plat et s'étend horizontalement uniquement, ma génération à lieu à la fois horizontalement et verticalement. 
Ce qui augmente énormément la complexité de la génération, car au lieu d'avoir des chunks carrés, ce seront des cubes.

Mon premier défi était de pouvoir générer du terrain sur de longues distances. Car, si tout les chunks ont la même taille, le nombre d'objets instancié devient beaucoup trop important pour que la machine supporte la pression.

Ainsi plus les chunks seront éloigné, plus les chunks seront grand. Ceci permet également de gérer le détail au sein de chaque chunk, car il n'est pas nécessaire d'avoir un terrain détaillé si celui-ci est à des kilomètres du joueur.

Cependant, une faiblesse de système est qu'il nécessite une génération de terrain presque constante, en effet, à moins que les chunks aient la même taille, les données déjà calculées ne sont pas réutilisables.

### Génération par îles

Ceci est un nouveau système que je suis actuellement en train de mettre en place.

Le but serait, au lieu de gérer le monde dans sa globalité de gérer les îles et leur génération individuellement.
Le but est donc de pouvoir générer les îles indépendamment avec ses différents niveaux de détails puis de les placer dans le monde.

Le sujet étant encore en phase d'expérimentation, je me retiens avant de m'avancer sur mes explications.



